package com.ahmad.usbserial.ffinal;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ahmad.usbserial.R;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView txt;
    Button btn1;
    Button btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt = (TextView) findViewById(R.id.txt);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ActivityDevices.class));
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ActivityExample.class));
            }
        });

        // Find all available drivers from attached devices.
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> devices = manager.getDeviceList();
        Iterator iterator = devices.values().iterator();
        txt.setText("manager.getDeviceList():  " + manager.getDeviceList() + "\n" + txt.getText());
        if(!iterator.hasNext()) {
            txt.setText("iterator.hasNext = false " + "\n" + txt.getText());
        }
        while(iterator.hasNext()) {
            txt.setText("iterator.hasNext = true " + "\n" + txt.getText());
            UsbDevice usbDevice = (UsbDevice) iterator.next();
            if(usbDevice != null) {
                txt.setText("getVendorId: " + usbDevice.getVendorId() + "\n" + txt.getText());

                IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                registerReceiver(mUsbReceiver, filter);

                manager = (UsbManager) getSystemService(Context.USB_SERVICE);
                PendingIntent mPermissionIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);
                manager.requestPermission(usbDevice, mPermissionIntent);

            }
        }




        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
        if (availableDrivers.isEmpty()) {
            txt.setText("availableDrivers isEmpty " + "\n" + txt.getText());
            return;
        }
        else {
            txt.setText("availableDrivers is not Empty " + "\n" + txt.getText());
            // Open a connection to the first available driver.
            UsbSerialDriver driver = availableDrivers.get(0);
            UsbDeviceConnection connection = manager.openDevice(driver.getDevice());
            if (connection == null) {
                txt.setText("connection is null " + "\n" + txt.getText());
                // You probably need to call UsbManager.requestPermission(driver.getDevice(), ..)

                PendingIntent mPermissionIntent = PendingIntent.getBroadcast(MainActivity.this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                UsbManager mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                mUsbManager.requestPermission(driver.getDevice(), mPermissionIntent);
                txt.setText("requestPermission " + "\n" + txt.getText());
            }
            else {
                txt.setText("connection is not null " + "\n" + txt.getText());

                PendingIntent mPermissionIntent = PendingIntent.getBroadcast(MainActivity.this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                UsbManager mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                mUsbManager.requestPermission(driver.getDevice(), mPermissionIntent);
                txt.setText("requestPermission " + "\n" + txt.getText());
            }

        }


        ((Button) findViewById(R.id.btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt.setText("button clicked " + "\n" + txt.getText());
                click(view);
            }
        });

    }

    private void click(View v) {
        try {
            ss();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private void ss() throws IOException {
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        ArrayList<UsbDevice> devices = new ArrayList<>();
        devices.addAll(manager.getDeviceList().values());
        UsbSerialProber usbSerialProber = UsbSerialProber.getDefaultProber();
        txt.setText("devices.size(): " + devices.size() + "\n" + txt.getText());
        if(devices.size() > 0) {
            txt.setText("devices.size() > 0 " + "\n" + txt.getText());
            UsbSerialDriver driver = usbSerialProber.probeDevice(devices.get(0));

            // Read some data! Most have just one port (port 0).
            UsbSerialPort port = driver.getPorts().get(0);

            UsbDeviceConnection connection = manager.openDevice(driver.getDevice());
            if (connection != null) {
                txt.setText("connection != null " + "\n" + txt.getText());
                try {
                    port.open(connection);
                    port.setParameters(19200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);


                    String text = "{\"Command\": {\"Model\": \"MP9407\",\"cstype\": \"INIT\",\"ctype\": \"CMD\" \"dtype\": \"Diagnostics\",\"Lang\": \"English\",\"serial\":\"MPS9407 951113 001\"}}";
                    byte buffer[] = text.getBytes();
                    port.write(buffer, 1000);

                    txt.setText("wrrritiiing" + "\n" + txt.getText());

//            int numBytesRead = port.read(buffer, 1000);
//            Log.d("dfgdf", "Read " + numBytesRead + " bytes.");
                } catch (IOException e) {
                    // Deal with error.
                } finally {
                    port.close();
                }
            } else {
                txt.setText("connection is nulllll " + "\n" + txt.getText());
            }
        }
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            txt.setText("Permission Granted " + "\n" + txt.getText());
        }
    };
}
