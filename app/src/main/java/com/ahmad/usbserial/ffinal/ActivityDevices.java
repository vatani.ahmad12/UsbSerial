package com.ahmad.usbserial.ffinal;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmad.usbserial.R;
import com.felhr.usbserial.CDCSerialDevice;
import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

public class ActivityDevices extends AppCompatActivity {

    public ListView lv;
    public UsbManager manager;
    public UsbSerialProber usbSerialProber;
    public ArrayList<UsbDevice> devices;
    public ArrayAdapter<UsbDevice> adapter;

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);
        lv = (ListView) findViewById(R.id.lv);


        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        usbSerialProber = UsbSerialProber.getDefaultProber();


        devices = new ArrayList<>();
        devices.clear();
        devices.addAll(manager.getDeviceList().values());
        if (devices.isEmpty()) {
            devices.add(null);
        }

        lv.setAdapter(new Adapter(this, devices));

    }


    class Adapter extends ArrayAdapter<UsbDevice> {

        ActivityDevices activityDevices;
        ArrayList<UsbDevice> devices;
        UsbDevice usbDevice;

        public Adapter(ActivityDevices activityDevices, ArrayList<UsbDevice> devices) {
            super(getApplicationContext(), R.layout.row_devices, devices);
            this.devices = devices;
            this.activityDevices = activityDevices;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_devices, parent, false);

            LinearLayout ll = (LinearLayout) rowView.findViewById(R.id.ll);
            TextView txtTitle = (TextView) rowView.findViewById(R.id.txtTitle);
            TextView txtDesc = (TextView) rowView.findViewById(R.id.txtDesc);
            usbDevice = (UsbDevice) this.devices.get(position);
            if (usbDevice == null) {
                txtTitle.setText("No deviecs");
                txtDesc.setText("");
            } else {
                String a = C0881f.m5046a(usbDevice);
                String b = C0881f.m5050b(usbDevice);
                String c = C0881f.m5052c(usbDevice);
                UsbSerialDriver probeDevice = activityDevices.usbSerialProber.probeDevice(usbDevice);
                if (probeDevice == null) {
//                    int b2 = C0881f.m5049b(usbDevice, this.f2593a.set);
//                    if (b2 > 0 && b2 < C0881f.f2639a.length) {
//                        c = C0881f.f2639a[b2];
//                    }
                }
                if (probeDevice != null) {
                    txtTitle.setText("Serial - " + c);
                } else if (c != null) {
                    txtTitle.setText("Custom - " + c);
                } else {
                    txtTitle.setText("Unknown");
                }
                Locale locale = Locale.US;
                c = "   VendorID: %04X   %s\n   ProductID: %04X   %s\n";
                Object[] objArr = new Object[4];
                objArr[0] = Integer.valueOf(usbDevice.getVendorId());
                objArr[1] = a != null ? a : "Unknown";
                objArr[2] = Integer.valueOf(usbDevice.getProductId());
                objArr[3] = b != null ? b : "Unknown";
                txtDesc.setText(String.format(locale, c, objArr));
            }

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(usbDevice != null) {
                        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                        registerReceiver(mUsbReceiver, filter);

                        PendingIntent mPermissionIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);
                        manager.requestPermission(usbDevice, mPermissionIntent);
                    }
                }
            });

            return rowView;
        }


        private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                final UsbSerialDevice serialPort = UsbSerialDevice.createUsbSerialDevice(usbDevice, manager.openDevice(usbDevice));
                if (serialPort != null) {
                    Toast.makeText(getApplicationContext(), "SerialPort is not null", Toast.LENGTH_SHORT).show();
                    if (serialPort.open()) {
                        Toast.makeText(getApplicationContext(), "SerialPort opened", Toast.LENGTH_SHORT).show();

                        Dialog dialog = new Dialog(activityDevices);
                        dialog.setContentView(R.layout.dialog_send_text);

                        final EditText editText = (EditText) dialog.findViewById(R.id.edit);
                        final Button button = (Button) dialog.findViewById(R.id.btn);

                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String text = editText.getText().toString();

                                serialPort.setBaudRate(9600);
                                serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                                serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                                serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                                /**
                                 * Current flow control Options:
                                 * UsbSerialInterface.FLOW_CONTROL_OFF
                                 * UsbSerialInterface.FLOW_CONTROL_RTS_CTS only for CP2102 and FT232
                                 * UsbSerialInterface.FLOW_CONTROL_DSR_DTR only for CP2102 and FT232
                                 */
                                serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                                byte buffer[] = text.getBytes();
                                serialPort.write(buffer);
                                serialPort.read(mCallback);
                            }
                        });

                        dialog.show();


//                        serialPort.getCTS(ctsCallback);
//                        serialPort.getDSR(dsrCallback);

                        //
                        // Some Arduinos would need some sleep because firmware wait some time to know whether a new sketch is going
                        // to be uploaded or not
                        //Thread.sleep(2000); // sleep some. YMMV with different chips.

                        // Everything went as expected. Send an intent to MainActivity
//                        Intent intent = new Intent(ACTION_USB_READY);
//                        context.sendBroadcast(intent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "SerialPort not opened", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "SerialPort is null", Toast.LENGTH_SHORT).show();
                }


            }
        };

        private UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {
            @Override
            public void onReceivedData(byte[] arg0) {
                try {
                    String data = new String(arg0, "UTF-8");
                    Toast.makeText(getApplicationContext(), "data: " + data, Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        };
    }

}
